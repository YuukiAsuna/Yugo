<?php
/**
 * Yugo: reusable php components
 * Copyright (c) Lotta Meow (http://github.com/lottietoday)
 *
 * For full copyright and licence information, please see the LICENCE
 * Redistributions of files must retain the above copyright notice
 *
 * @copyright Copyright (c) Lotta Meow (http://github.com/lottietoday)
 * @link https://github.com/lottietoday/Yugo The Yugo project
 * @since 0.1.0
 * @licence https://github.com/lottietoday/Yugo/blob/master/LICENSE
 *
 * @author Lotta
 * @created 6.2.16 20:06
 */

//Directory Separator
define('DS', '/');

define('YUGO_TPL_ROOT', $_SERVER["DOCUMENT_ROOT"].'/app/Tpl/');

define('LOTTE_WWW_ROOT', $_SERVER['DOCUMENT_ROOT'].'/wwwroot/');

define('LOTTE_VENDOR', $_SERVER['DOCUMENT_ROOT'].'/vendor/');

define('LOTTE_CTRLS', $_SERVER['DOCUMENT_ROOT'].'/src/');