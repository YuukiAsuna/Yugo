<?php
/**
 * Yugo: reusable php components
 * Copyright (c) Lotta Meow (http://github.com/lottietoday)
 *
 * For full copyright and licence information, please see the LICENCE
 * Redistributions of files must retain the above copyright notice
 *
 * @copyright Copyright (c) Lotta Meow (http://github.com/lottietoday)
 * @link https://github.com/lottietoday/Yugo The Yugo project
 * @since 0.1.0
 * @licence https://github.com/lottietoday/Yugo/blob/master/LICENSE
 *
 * @author Martin
 * @created 14.2.16 17:09
 */

/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 14. 2. 2016
 * Time: 17:09
 */

$data = [
    'Hostname' => 'nameOfHost',
    'Env' => ['Foo=Bar', 'Baz=quux'],
    'Image' => 'drupal:8.0.3',
    'HostConfig' => [
        'PortBindings' => [
            '80/tcp' => [
                ["HostPort" => '']
            ]
        ]
    ]
];

$json = json_encode($data);

?>

<pre><?php var_dump($json); ?></pre>
