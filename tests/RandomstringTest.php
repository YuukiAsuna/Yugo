<?php
/**
 * Yugo: reusable php components
 * Copyright (c) Lotta Meow (http://github.com/lottietoday)
 *
 * For full copyright and licence information, please see the LICENCE
 * Redistributions of files must retain the above copyright notice
 *
 * @copyright Copyright (c) Lotta Meow (http://github.com/lottietoday)
 * @link https://github.com/lottietoday/Yugo The Yugo project
 * @since 0.1.0
 * @licence https://github.com/lottietoday/Yugo/blob/master/LICENSE
 *
 * @author Martin
 * @created 7.2.16 17:00
 */

/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 7. 2. 2016
 * Time: 17:00
 */

class RandomstringTest extends PHPUnit_Framework_TestCase
{
    public function testString() {
        require_once '/config/app.php';
        require_once '/src/Yugo/Randomstring.php';
        $s = new \Yugo\Randomstring();

        $a = strlen($s->generate(12));

        $this->assertEquals(12, $a);
    }
}
