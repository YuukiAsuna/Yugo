<?php
/**
 * Yugo: reusable php components
 * Copyright (c) Lotta Meow (http://github.com/lottietoday)
 *
 * For full copyright and licence information, please see the LICENCE
 * Redistributions of files must retain the above copyright notice
 *
 * @copyright Copyright (c) Lotta Meow (http://github.com/lottietoday)
 * @link https://github.com/lottietoday/Yugo The Yugo project
 * @since 0.1.0
 * @licence https://github.com/lottietoday/Yugo/blob/master/LICENSE
 *
 * @author Martin
 * @created 7.2.16 17:10
 */
require_once '/src/Yugo/Shortener.php';
/**
 * Created by PhpStorm.
 * User: Martin
 * Date: 7. 2. 2016
 * Time: 17:10
 */
class ShortenerTest extends PHPUnit_Framework_TestCase
{

    public function testOneUndred() {
        $s = new \Yugo\Shortener();
        $string = $s->short(100);
        $this->assertEquals('1C', $string);
    }

    public function testOneThousand() {
        $s = new \Yugo\Shortener();
        $string = $s->short(1000);
        $this->assertEquals('g8', $string);
    }

    public function testOneMilion() {
        $s = new \Yugo\Shortener();
        $string = $s->short(10000221232132100);
        $this->assertEquals('4c92', $string);
    }
}
