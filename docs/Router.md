# Router

```
use Yugo\Router;
```

Router class will return path to file from string

```
$router = new Router();

$page = $router->route('folder/action'); //returns array
```

will return array page with

```
$page['folder'] = WWW_ROOT./folder
$page['action'] = action
$page['page_route']= WWW_ROOT./folder/action.flask.html
```

If there is no string on input it will returns default page ```WWW_ROOT./index.flask.html```