# Random string generator

```
use Yugo\Randomstring;
```

This class will return you random generated string. For now its dont allowing change codeset easy when you construct class.

To generate string use following in your controller

```
$str = new Randomstring();

echo "App_id: {$str->generate(20)} <br />";
echo "App_secret: {$str->generate(40)} <br />";
```