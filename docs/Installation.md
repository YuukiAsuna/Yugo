# Installation

Recomended download is with composer

```
composer require yugo/yugo
```

or add to your composer.json foloving lines

```
"require": {
    "yugo/yugo": "dev-master"
 }
```

Yugo does not support autoload for now, to make it working include yugo.php in your project. For example

```
require_once __DIR__.'/../src/yugo.php';
```

All important variables for yugo are in config/app.php for now it using foloving global variable

```
define('YUGO_TPL_ROOT', $_SERVER["DOCUMENT_ROOT"].'/app/Tpl/');

define('LOTTE_WWW_ROOT', $_SERVER['DOCUMENT_ROOT'].'/wwwroot/');

define('LOTTE_VENDOR', $_SERVER['DOCUMENT_ROOT'].'/vendor/');
```

You can create your config file or copy one from repository to your root and include it.