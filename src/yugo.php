<?php
/**
 * Yugo: reusable php components
 * Copyright (c) Lotta Meow (http://github.com/lottietoday)
 *
 * For full copyright and licence information, please see the LICENCE
 * Redistributions of files must retain the above copyright notice
 *
 * @copyright Copyright (c) Lotta Meow (http://github.com/lottietoday)
 * @link https://github.com/lottietoday/Yugo The Yugo project
 * @since 0.1.0
 * @licence https://github.com/lottietoday/Yugo/blob/master/LICENSE
 *
 * @author Lotta
 * @created 3.2.16 20:34
 */

require_once $_SERVER["DOCUMENT_ROOT"].'/config/app.php';

spl_autoload_register(function($class_name) {
    static $map = [
        'Yugo' => 'yugo.php',
        'Yugo\Controller' => 'Controller.php',
        'Yugo\Shortener' => 'Shortener.php',
        'Yugo\Ccvalidator' => 'Ccvalidator.php',
        'Yugo\Router' => 'Router.php',
        'Yugo\Tpl\Engine' => 'Tpl/Engine.php',
        'Yugo\Randomstring' => 'Randomstring.php'
    ];
    
    if (isset($map[$class_name])) {
		require __DIR__ . '/Yugo/' . $map[$class_name]; //to load from any folder...
	}
});