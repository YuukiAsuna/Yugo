<?php

namespace Yugo;

class Shortener extends Controller{
    
    private $_defaultConfig;
    
    public function __construct() {
        $this->_defaultConfig = [
            'codeset' => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
        ];
    }

    /**
     * Short Method shorten given id and returnning string based on codeset
     * @param null $idemID
     * @return string Shortened ID
     */
    public function short($idemID = null) {
        return $this->_shortenNumber($idemID);
    }

    /**
     * Long Method decode string in input and reurns original ID
     * @param null $shortenString
     * @return bool|int Decoded shortened string
     */
    public function long($shortenString = null) {
        return $this->_decodeString($shortenString);
    }

    /**
     * _shortenNumber method
     * @param $number
     * @return string Shortened ID
     */
    private function _shortenNumber($number) {
        //$config = $this->_defaultConfig;
        $codeset = $this->_defaultConfig['codeset'];

        $base = strlen($codeset);
        $n = $number;
        $converted = "";

        while ($n > 0) {
            $converted = substr($codeset, ($n % $base), 1) . $converted;
            $n = floor($n/$base);
        }

        return $converted;
    }

    /**
     * _decodeString Method
     * @param $conv
     * @return bool|int Decoded shortened string
     */
    private function _decodeString($conv) {
        //$config = $this->_defaultConfig;
        $codeset = $this->_defaultConfig['codeset'];

        $base = strlen($codeset);
        $converted = $conv;
        $c = 0;
        for ($i = strlen($converted); $i; $i--) {
            $c += strpos($codeset, substr($converted, (-1 * ( $i - strlen($converted) )),1))
                * pow($base,$i-1);
        }

        return $c;
    }
}