<?php
/**
 * Yugo: reusable php components
 * Copyright (c) Lotta Meow (http://github.com/lottietoday)
 *
 * For full copyright and licence information, please see the LICENCE
 * Redistributions of files must retain the above copyright notice
 *
 * @copyright Copyright (c) Lotta Meow (http://github.com/lottietoday)
 * @link https://github.com/lottietoday/Yugo The Yugo project
 * @since 0.1.0
 * @licence https://github.com/lottietoday/Yugo/blob/master/LICENSE
 *
 * @author Lotta
 * @created 6.2.16 21:03
 */

namespace Yugo;

class Router extends Controller{

    public function route ($route = null) {
        if ($route == null) {
            // if there is no call then return standard page
            return $page = [
                'folder' => LOTTE_WWW_ROOT,
                'action'  => 'index.flask.html',
                'page_route' => LOTTE_WWW_ROOT.DS.'index.flask.html'
            ];
        }

        $requested_address = explode('/',$route);
        if (isset($requested_address[1])) {
            // if route calling domain/page this will return route to page in forlder www root / www domain
            $page = [
                'folder' => LOTTE_WWW_ROOT.$requested_address[0],
                'action' => $requested_address[1],
                'page_route' => LOTTE_WWW_ROOT.DS.$requested_address[0].DS.$requested_address[1].'.flask.html',
                'Controller' => LOTTE_CTRLS.$requested_address[0].'Controller'.'.php',
                'Class' => '\Controller\\'.$requested_address[0].'Controller',
                'Action' => $requested_address[1]
            ];
        } else {
            //return page in www root folder
            $page = [
                'folder' => LOTTE_WWW_ROOT,
                'action' => $requested_address[0],
                'page_route' => LOTTE_WWW_ROOT.DS.$requested_address[0].'.flask.html'
            ];
        }
        return $page;
    }
}