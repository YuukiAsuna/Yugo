<?php
/**
 * Yugo: reusable php components
 * Copyright (c) Lotta Meow (http://github.com/lottietoday)
 *
 * For full copyright and licence information, please see the LICENCE
 * Redistributions of files must retain the above copyright notice
 *
 * @copyright Copyright (c) Lotta Meow (http://github.com/lottietoday)
 * @link https://github.com/lottietoday/Yugo The Yugo project
 * @since 0.1.0
 * @licence https://github.com/lottietoday/Yugo/blob/master/LICENSE
 *
 * @author Lotta
 * @created 3.2.16 20:37
 */

namespace Yugo\DB;

class Db {
    
    private $_defaultConfig = [
        'db_server' => 'localhost',
        'db_port' => '3306',
        'db_username' => 'appname',
        'db_database' => 'appname',
        'db_password' => 'password',
    ];
    
    public function __construct($configuration) {
        $this->_defaultConfig = $configuration;
    }

    //TODO implement!!!
    public function find($entity) {
        
    }
}