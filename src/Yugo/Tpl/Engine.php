<?php

namespace Yugo\Tpl;

class Engine {
    
     /* Data stored by TPL
     * @var array
     */
    private $_data = array();   
     
    /**
     * Data used by the view (overwrite the $_data)
     * @var array
     */
    private $_dataView = array();
 
    /**
     * magic method set
     */         
    public function __set($key, $value) 
    {
        $this->_data[$key] = $value;
    }
      
    /**
     * magic method get
     */   
    public function __get($key) 
    {
        if(isset($this->_dataView[$key])) {  
            return $this->_dataView[$key];
        }
        else if(isset($this->_data[$key])) {
            return $this->_data[$key];
        }
        else {
            return false;
        }
    }

    /**
     * render method - This method is used for render page from template and data
     * @param $filename
     * @param null $data
     * @return string
     */
    public function render($filename, $data = null) {
        //$fileName = YUGO_TPL_ROOT.$filename;
        //updated to load filename returned from router
        $fileName = $filename;
        if($data) {
            $this->_dataView = $data;
        }
        $rendered = "";
        if(file_exists($fileName)) {
            ob_start();
            require($fileName);   
            $rendered = ob_get_contents(); 
            ob_end_clean();         
        }
        
        $this->_dataView = array();
        
        return $rendered;
    }
}