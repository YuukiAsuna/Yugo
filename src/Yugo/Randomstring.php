<?php
/**
 * Yugo: reusable php components
 * Copyright (c) Lotta Meow (http://github.com/lottietoday)
 *
 * For full copyright and licence information, please see the LICENCE
 * Redistributions of files must retain the above copyright notice
 *
 * @copyright Copyright (c) Lotta Meow (http://github.com/lottietoday)
 * @link https://github.com/lottietoday/Yugo The Yugo project
 * @since 0.1.0
 * @licence https://github.com/lottietoday/Yugo/blob/master/LICENSE
 *
 * @author Lotta
 * @created 3.2.16 20:35
 */

namespace Yugo;

class Randomstring extends Controller{

    /**
     * @var array
     */
    private $_defaultConfig = [
        'codeset' => '0123456789abcdef'
    ];

    /**
     * @param $length
     * @return string
     */
    public function generate($length) {
        $codeset = $this->_defaultConfig['codeset'];
        $CodesetLength = strlen($codeset) -1;
        $generatedString = '';

        for ($i = 0; $i < $length; $i++) {
            $generatedString .= $codeset[rand(0, $CodesetLength - 1)];
        }

        return $generatedString;
    }
}