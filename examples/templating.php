<?php

namespace examples\templating;

require_once __DIR__.'/../src/yugo.php';

use Yugo\Tpl\Engine;

$t = new Engine();

$t->meno = 'Lotta';
$t->priezvisko = 'Meow.';
$t->header = 'Yugo - Flask template engine';
echo $t->render('ahoj.flask.php');