<?php

require_once __DIR__.'/../src/yugo.php';

use Yugo\Ccvalidator;

$ccv = new Ccvalidator();

$result = $ccv->CreditCard('5330 4171 3521 4522');

echo $result['valid'] ? $result['ccnum'].' - '.$result['type'] : 'Not valid';