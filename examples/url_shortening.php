<?php

namespace examples\UrlShortening;

require_once __DIR__.'/../src/yugo.php';

use Yugo\Shortener;

$shortener = new Shortener();

echo '252564565487936 after shortening '.$shortener->short(252564565487936);

phpinfo();