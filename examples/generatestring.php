<?php
/**
 * Yugo: reusable php components
 * Copyright (c) Lotta Meow (http://github.com/lottietoday)
 *
 * For full copyright and licence information, please see the LICENCE
 * Redistributions of files must retain the above copyright notice
 *
 * @copyright Copyright (c) Lotta Meow (http://github.com/lottietoday)
 * @link https://github.com/lottietoday/Yugo The Yugo project
 * @since 0.1.0
 * @licence https://github.com/lottietoday/Yugo/blob/master/LICENSE
 *
 * @author Lotta
 * @created 3.2.16 20:35
 */

namespace examples\generatestring;

require_once __DIR__.'/../src/yugo.php';

use Yugo\Randomstring;

$str = new Randomstring();

echo "App_id: {$str->generate(20)} <br />";
echo "App_secret: {$str->generate(40)} <br />";